package projectcodehandling

import (
	_ "embed"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"unicode"
	"unicode/utf8"
)

var mappingFiles map[string]string

//go:embed data/EnglishAnimalNames.txt
var XmappingEnglishAnimals string

//go:embed data/GreekInspiredAlphabet.txt
var XmappingGreekInspired string

//go:embed data/NATOPhoneticAlphabet.txt
var XmappingNato string

func init() {
	mappingFiles = map[string]string{
		"EnglishAnimals": "EnglishAnimalNames.txt",
		"GreekInspired":  "GreekInspiredAlphabet.txt",
		"Nato":           "NATOPhoneticAlphabet.txt",
		"PeopleRhodan":   "PeopleRhodan.txt",
	}
}

type PhoneticMapping struct {
	Name string
	m    map[rune]string
}

type PhoneticMappingsLib struct {
	Lib map[string]PhoneticMapping
}

func CreatePhoneticMappingsLib(datafilesPath string) *PhoneticMappingsLib {
	res := PhoneticMappingsLib{
		Lib: make(map[string]PhoneticMapping),
	}

	defaultPhoneticMapping := PhoneticMapping{
		Name: "Default",
		m:    make(map[rune]string, 26),
	}
	for _, r := range "abcdefghijklmnopqrstuvwxyz" {
		defaultPhoneticMapping.m[r] = string(rune(r))
	}

	res.Lib[defaultPhoneticMapping.Name] = defaultPhoneticMapping
	if len(datafilesPath) > 0 {
		for m, f := range mappingFiles {
			mfilePath := filepath.Join(datafilesPath, f)
			pm, err := loadPhoneticMapping(mfilePath)
			if err != nil {
				log.Printf("Could not load phonetic mapping from '%s'\n", mfilePath)
				continue
			}

			res.Lib[m] = *pm
		}
	} else {
		res.Lib["EnglishAnimals"] = *parsePhoneticMapping("EnglishAnimals", XmappingEnglishAnimals)
		res.Lib["GreekInspired"] = *parsePhoneticMapping("GreekInspired", XmappingGreekInspired)
		res.Lib["Nato"] = *parsePhoneticMapping("Nato", XmappingNato)
	}

	return &res
}

func (pml *PhoneticMappingsLib) GetPhonetic(mapping string, symbol rune) string {
	pm, ok := pml.Lib[mapping]
	if !ok {
		log.Printf("Could not find phonetic mapping '%s'\n", mapping)
		return string(unicode.ToUpper(symbol))
	}

	lcaseSym := unicode.ToLower(symbol)

	res, ok := pm.m[lcaseSym]
	if !ok {
		log.Printf("Could not find '%c' in mapping\n", lcaseSym)
		// for r, s := range pm.m {
		// 	fmt.Printf("    '%c' -> %s / %s\n", r, s, pm.m[r])
		// }
		return string(unicode.ToUpper(symbol))
	}

	return res
}

//#region Private functions

func loadPhoneticMapping(filename string) (*PhoneticMapping, error) {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		fmt.Printf("file %s does not exist\n", filename)
		return nil, fmt.Errorf("Could not load file '%s'", filename)
	}

	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Printf("file %s could not be read\n", filename)
		return nil, fmt.Errorf("Could not load file '%s'", filename)
	}
	res := parsePhoneticMapping(filepath.Base(filename), string(buf))

	return res, nil
}

func parsePhoneticMapping(name string, rawMapping string) *PhoneticMapping {
	res := PhoneticMapping{
		Name: name,
	}
	res.m = make(map[rune]string)

	for _, line := range strings.Split(rawMapping, "\n") {
		if len(strings.TrimSpace(line)) == 0 {
			continue
		}
		// log.Printf("  > %s", line)
		tokens := strings.Split(line, ",")
		r, _ := utf8.DecodeRuneInString(strings.ToLower(strings.TrimSpace(tokens[0])))
		res.m[r] = strings.TrimSpace(tokens[1])
	}

	return &res
}

//#endregion Private functions
