// The projectcodehandling package contains all functions required to generate codes based on some
// data and create a pronouncable code name from this.
package projectcodehandling

import (
	"crypto/md5"
	"encoding/hex"
	"log"
	"projectcodes/projectdatahandling"
)

// Method to generate codes for a project (given its project infos).
// The dataPath is where phonetic mapping files etc. are expected.
// A list of the generated codes is returned.
func GenerateCodesForProjectInfo(dataPath string, pi projectdatahandling.ProjectInfo) []Code {
	stringToEncode := pi.ProjectNumber + pi.WorkingTitle // fmt.Sprintf("Project number: %s\r\nWorking title: %s", pi.ProjectNumber, pi.WorkingTitle)
	log.Printf("Data to encode:\n%s", stringToEncode)

	buf := []byte(stringToEncode)
	hash := Hash(calcHash(buf))
	res := hash.generateCodesList(dataPath)

	return res
}

// Function to generate codes for a given data string.
// The dataPath is where phonetic mapping files etc. are expected, while data
// contains the string which serves as the basis for the code generation.
// A list of the generated codes is returned.
func GenerateCodesForString(dataPath string, data string) []Code {
	buf := []byte(data)
	hash := Hash(calcHash(buf))
	res := hash.generateCodesList(dataPath)

	return res
}

//region Private functions

func calcHash(buf []byte) []byte {
	res := md5.Sum(buf)

	return res[:]
}

func (hash Hash) generateCodesList(dataPath string) []Code {
	pmlib := CreatePhoneticMappingsLib(dataPath)

	res := []Code{
		{Type: "Raw Hash", Code: hex.EncodeToString(hash)},
		{Type: "Category 1", Code: hash.createCategoryCode1(pmlib)},
		{Type: "Category 2", Code: hash.createCategoryCode2(pmlib)},
		{Type: "Project 1", Code: hash.createProjectCode1(pmlib)},
		{Type: "Project 2", Code: hash.createProjectCode2(pmlib)},
		{Type: "Project 3", Code: hash.createProjectCode3(pmlib)},
	}

	return res
}

//endregion Private functions
