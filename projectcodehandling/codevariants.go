package projectcodehandling

import (
	"fmt"
)

// Create the defined variant 1 for category codes from a hash.
// This codes are of the form  "GREEK_LETTER DIGIT" (e.g. "Alpha 8")
func (h Hash) createCategoryCode1(pml *PhoneticMappingsLib) string {
	p1 := MapSymbolToAlphaDigitSpace(h[0])
	p1x := pml.GetPhonetic("GreekInspired", p1)
	p2 := MapSymbolToDigit(h[1])
	res := fmt.Sprintf("%s %c", p1x, p2)

	return res
}

// Create the defined variant 2 for category codes from a hash.
// This codes are of the form  "GREEK_LETTER DIGIT" (e.g. "Alpha 8")
func (h Hash) createCategoryCode2(pml *PhoneticMappingsLib) string {
	p1 := MapSymbolToAlphaDigitSpace(h[2])
	p1x := pml.GetPhonetic("GreekInspired", p1)
	p2 := MapSymbolToDigit(h[3])
	res := fmt.Sprintf("%s %c", p1x, p2)

	return res
}

func (h Hash) createProjectCode1(pml *PhoneticMappingsLib) string {
	p1 := MapSymbolToAlphaDigitSpace(h[0])
	p1x := pml.GetPhonetic("Nato", p1)
	p2 := MapBinStringToNumber(h[1:5])
	p3 := MapBinStringToNumber(h[5:9])

	res := fmt.Sprintf("%s %s [%s]", p1x, p2, p3)

	return res
}

func (h Hash) createProjectCode2(pml *PhoneticMappingsLib) string {
	p1 := MapSymbolToAlphaDigitSpace(h[0])
	p1x := pml.GetPhonetic("Nato", p1)
	p2 := MapSymbolToAlphaDigitSpace(h[1])
	p2x := pml.GetPhonetic("Nato", p2)
	p3 := MapBinStringToNumber(h[2:5])
	p4 := MapBinStringToNumber(h[5:8])

	res := fmt.Sprintf("%s %s %s [%s]", p1x, p2x, p3, p4)

	return res
}

func (h Hash) createProjectCode3(pml *PhoneticMappingsLib) string {
	p1 := MapSymbolToAlphaDigitSpace(h[0])
	p1x := pml.GetPhonetic("Nato", p1)
	p2 := MapSymbolToAlphaDigitSpace(h[1])
	p2x := pml.GetPhonetic("GreekInspired", p2)
	p3 := MapBinStringToNumber(h[2:5])
	p4 := MapBinStringToNumber(h[5:8])

	res := fmt.Sprintf("%s %s %s [%s]", p1x, p2x, p3, p4)

	return res
}
