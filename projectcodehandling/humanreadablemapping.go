package projectcodehandling

import (
	"bytes"
	"fmt"
	"unicode"
	"unicode/utf8"
)

//#region Mapping functions for single characters

// Map any integer number to a lower-case character.
// The number to map is provided as n, basically the modulo with 26 is
// calculated and mapped to a-z.
func MapNumberToAlphaChar(n int) rune {
	dist := int32(n) % 26

	return (int32('a') + dist)
}

// Transform a byte into a readable standard character.
// The idea is to keep the byte as is if it is already a ASCII letter or a digit,
// otherwise it is mapped to one of those and returned as a rune. A special case
// is if the byte represents a space, in which case '*' is returned.
func MapSymbolToAlphaDigitSpace(b byte) rune {
	// if unicode.IsLetter(rune(b)) {
	if isValidAsciiLetter(rune(b)) {
		return rune(b)
	} else if unicode.IsSpace(rune(b)) {
		return '*'
	} else {
		return MapNumberToAlphaChar(int(b))
	}
}

// Transform a byte to a single digit.
// Similar to MapSymbolToAlphaDigitSpace(), the provided byte is mapped to a
// single digit which is returned as a rune.
func MapSymbolToDigit(b byte) rune {
	if unicode.IsDigit(rune(b)) {
		return rune(b)
	}

	r, _ := utf8.DecodeRuneInString(fmt.Sprintf("%d", b%10))
	return r
}

func MapBinStringToNumber(bs []byte) string {
	var buf bytes.Buffer

	for _, b := range bs {
		buf.WriteRune(MapSymbolToDigit(b))
	}

	return buf.String()
}

//#endregion Mapping functions for single characters

//#region Private functions

func isValidAsciiLetter(r rune) bool {
	i := int(r)

	return (i >= 'a' && i <= 'z') || (i >= 'A' && i <= 'Z')
}

//#endregion Private functions
