package projectcodehandling

// Define Hash as a byte slice.
type Hash []byte

// Represent a Code as a combination of its code type (a string name) and the
// actual code.
type Code struct {
	Type string
	Code string
}
