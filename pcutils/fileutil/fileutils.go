package fileutil

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
)

func FileExists(filename string) bool {
	if _, err := os.Stat(filename); err == nil {
		return true
	} else if errors.Is(err, os.ErrNotExist) {
		return false
	} else {
		log.Printf("File '%s' may or may not exist, error: %o", filename, err)
		return false
	}
}

func CopyFile(src string, dest string) error {
	bytesRead, err := ioutil.ReadFile(src)

	if err != nil {
		return err
	}

	err = ioutil.WriteFile(dest, bytesRead, 0644)

	if err != nil {
		return err
	}

	return nil
}
