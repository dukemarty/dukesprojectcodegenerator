package archiveutil

import (
	"archive/zip"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func ExtractZipContents(zipfile string, destinationFolder string) error {
	arch, err := zip.OpenReader(zipfile)
	if err != nil {
		return err
	}
	defer arch.Close()

	err = os.Mkdir(destinationFolder, 0644)
	if err != nil {
		return err
	}
	// Iterate through the files in the archive,
	// storing each and every single file.
	for _, f := range arch.File {
		if f.FileInfo().IsDir() {
			newDir := filepath.Join(destinationFolder, f.Name)
			err := os.MkdirAll(newDir, 0644)
			if err != nil {
				return err
			}
		} else {
			dir := filepath.Dir(f.Name)
			err := os.MkdirAll(filepath.Join(destinationFolder, dir), 0644)
			if err != nil {
				return err
			}

			compFile, err := f.Open()
			if err != nil {
				return err
			}
			content, err := io.ReadAll(compFile)
			if err != nil {
				return err
			}
			err = os.WriteFile(filepath.Join(destinationFolder, f.Name), content, 0644)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Following https://stackoverflow.com/questions/37869793/how-do-i-zip-a-directory-containing-sub-directories-or-files-in-golang
func CompressFolderToZip(folder string, targetZipFile string) error {
	file, err := os.OpenFile(targetZipFile, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer file.Close()

	w := zip.NewWriter(file)
	defer w.Close()

	walker := func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}

		trimmedPath := strings.TrimPrefix(path, folder+"\\")

		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()

		// Ensure that `path` is not absolute; it should not start with "/".
		// This snippet happens to work because I don't use
		// absolute paths, but ensure your real-world code
		// transforms path into a zip-root relative path.
		f, err := w.Create(trimmedPath)
		if err != nil {
			return err
		}

		_, err = io.Copy(f, file)
		if err != nil {
			return err
		}

		return nil
	}
	err = filepath.Walk(folder, walker)
	if err != nil {
		panic(err)
	}

	return nil
}
