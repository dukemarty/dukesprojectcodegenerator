package projectcodemanagement

import (
	"encoding/csv"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

type storageBackendFiles struct {
	DataPath string
}

func (sbf *storageBackendFiles) Init(dataBasePath string) {
	sbf.DataPath = dataBasePath
}

func (sbf *storageBackendFiles) ReadCodetypes() ([]string, error) {
	entries, err := ioutil.ReadDir(sbf.DataPath)
	if err != nil {
		return nil, fmt.Errorf("Could not read data file entries from '%s'\n", sbf.DataPath)
	}

	codetypes := make([]string, 0, 10)
	for _, de := range entries {
		if de.IsDir() {
			// log.Printf("Skipping subdirectory '%s'\n", de.Name())
			continue
		}

		var filename = de.Name()
		if filepath.Ext(filename) != ".csv" {
			// log.Printf("Skipping '%s' as it is no CSV file\n", de.Name())
			continue
		}

		log.Printf("Found file: '%s'\n", filename)

		codetypes = append(codetypes, filename[:len(filename)-4])
	}

	return codetypes, nil
}

func (sbf *storageBackendFiles) ListCodes(codeType string) ([]CodeEntry, error) {
	cvsFilename := codeType + ".csv"
	dataFile := filepath.Join(sbf.DataPath, cvsFilename)

	filecontent, err := ioutil.ReadFile(dataFile)
	if err != nil {
		return nil, fmt.Errorf("Could not read data from '%s'\n", cvsFilename)
	}

	in := string(filecontent)
	r := csv.NewReader(strings.NewReader(in))

	records, err := r.ReadAll()
	if err != nil {
		return nil, err
	}

	entries := make([]CodeEntry, 0, 10)
	for _, r := range records {
		ce := CodeEntry{
			Entry: r[0],
			Code:  r[1],
		}
		entries = append(entries, ce)
	}

	return entries, nil
}

func (sbf *storageBackendFiles) AddCode(codeType string, entry string, value string) error {
	cvsFilename := codeType + ".csv"
	dataFile := filepath.Join(sbf.DataPath, cvsFilename)

	filecontent, err := ioutil.ReadFile(dataFile)
	var in string
	if err != nil {
		log.Printf("Could not read data from '%s'\n", cvsFilename)
		in = ""
	} else {
		in = string(filecontent)
	}

	r := csv.NewReader(strings.NewReader(in))

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	recordMap := make(map[string]string)
	for _, r := range records {
		recordMap[r[0]] = r[1]
	}

	recordMap[entry] = value

	records = make([][]string, 0, len(recordMap))
	for k, v := range recordMap {
		records = append(records, []string{k, v})
	}

	f, err := os.OpenFile(dataFile, os.O_CREATE|os.O_WRONLY, 0755)
	if err != nil {
		log.Fatalf("Could not open %s for writing the extended code data to!", dataFile)
	}
	defer f.Close()

	w := csv.NewWriter(f)
	w.WriteAll(records) // calls Flush internally

	return err
}

func (sbf *storageBackendFiles) RemoveCode(codeType string, entry string) error {
	cvsFilename := codeType + ".csv"
	dataFile := filepath.Join(sbf.DataPath, cvsFilename)

	filecontent, err := ioutil.ReadFile(dataFile)
	if err != nil {
		log.Printf("Could not read data from '%s' to remove '%s' entry\n", cvsFilename, entry)
		os.Exit(0)
	}

	in := string(filecontent)
	r := csv.NewReader(strings.NewReader(in))

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	if len(records) == 0 {
		os.Exit(0)
	}

	f, err := os.OpenFile(dataFile, os.O_CREATE|os.O_WRONLY, 0755)
	if err != nil {
		log.Fatalf("Could not open %s for writing the extended code data to!", dataFile)
	}
	defer f.Close()

	w := csv.NewWriter(f)

	for _, r := range records {
		if r[0] != entry {
			err := w.Write(r)
			if err != nil {
				log.Printf("Error when writing record %s/%s: %v", r[0], r[1], err)
			}
		}
	}

	w.Flush()

	return nil
}
