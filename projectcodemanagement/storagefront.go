package projectcodemanagement

import "strings"

type CodeEntry struct {
	Entry string
	Code  string
}

type storageBackend interface {
	Init(dataBasePath string)
	ReadCodetypes() (codeTypes []string, error error)
	ListCodes(codeType string) ([]CodeEntry, error)
	AddCode(codeType string, entry string, value string) error
	RemoveCode(codeType string, entry string) error
}

func getStore(backendType string, dataPath string) storageBackend {
	var res storageBackend = nil

	switch strings.ToUpper(backendType) {
	case "FILES":
		res = &storageBackendFiles{}
	case "SQLITE":
		res = &storageBackendSqlite{}
	}

	res.Init(dataPath)

	return res
}
