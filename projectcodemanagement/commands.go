package projectcodemanagement

func ReadCodeTypes(storageEngine string, dataPath string) ([]string, error) {
	storage := getStore(storageEngine, dataPath)
	codeTypes, err := storage.ReadCodetypes()

	return codeTypes, err
}

func ListCodes(storageEngine string, dataPath string, codeType string) ([]CodeEntry, error) {
	storage := getStore(storageEngine, dataPath)
	records, err := storage.ListCodes(codeType)

	return records, err
}

func AddCode(storageEngine string, dataPath string, codeType string, entry string, code string) error {
	storage := getStore(storageEngine, dataPath)
	err := storage.AddCode(codeType, entry, code)

	return err
}

func RemoveCode(storageEngine string, dataPath string, codeType string, entry string) error {
	storage := getStore(storageEngine, dataPath)
	err := storage.RemoveCode(codeType, entry)

	return err
}
