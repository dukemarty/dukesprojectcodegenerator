package projectcodemanagement

import (
	"database/sql"
	"fmt"
	"log"
	"path"

	_ "github.com/mattn/go-sqlite3"
)

const tableCodeTypes = "_CodeTypes"

type storageBackendSqlite struct {
	dataBasePath string
	DbFilePath   string
}

func (sbsq *storageBackendSqlite) Init(dataBasePath string) {
	sbsq.dataBasePath = dataBasePath
	sbsq.DbFilePath = path.Join(sbsq.dataBasePath, "CodesDatabase.sqlite3")
}

func (sbsq *storageBackendSqlite) ReadCodetypes() ([]string, error) {
	db, err := sbsq.openDb()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// checkErr(db.Ping())

	codeTypeRows, err := db.Query(fmt.Sprintf("SELECT * FROM %s", tableCodeTypes))
	if err != nil {
		log.Fatalf("Error occured when trying to read from %s table: %v", tableCodeTypes, err)
	}
	defer codeTypeRows.Close()

	codetypes := make([]string, 0, 10)
	for codeTypeRows.Next() {
		var name string

		err := codeTypeRows.Scan(&name)
		if err != nil {
			log.Printf("Error scanning row '%v': %v\n", codeTypeRows, err)
			continue
		}

		codetypes = append(codetypes, name)
	}

	return codetypes, nil
}

func (sbsq *storageBackendSqlite) ListCodes(codeType string) ([]CodeEntry, error) {
	db, err := sbsq.openDb()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	row := db.QueryRow(fmt.Sprintf("SELECT * FROM %s WHERE name == ?", tableCodeTypes), codeType)
	scanRes := row.Scan()
	if scanRes != nil && scanRes == sql.ErrNoRows {
		return nil, fmt.Errorf("Requested code type '%s' does not exist.", codeType)
	}

	codeRows, err := db.Query(fmt.Sprintf("SELECT * FROM %s", codeType))
	if err != nil {
		return nil, fmt.Errorf("Error occured when trying to query codes of type '%s': %v", codeType, err)
	}
	defer codeRows.Close()

	codes := make([]CodeEntry, 0, 10)
	for codeRows.Next() {
		var entry, code string
		err := codeRows.Scan(&entry, &code)
		if err != nil {
			log.Printf("Error scanning row: '%v': %v\n", codeRows, err)
			continue
		}

		codes = append(codes, CodeEntry{Entry: entry, Code: code})
	}

	return codes, nil
}

func (sbsq *storageBackendSqlite) AddCode(codeType string, entry string, value string) error {
	db, err := sbsq.openDb()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// checkErr(db.Ping())

	codeTypeRow := db.QueryRow("SELECT * FROM ? WHERE Name = ?", tableCodeTypes, codeType)

	var codeTypeRead string
	err = codeTypeRow.Scan(&codeTypeRead)
	if err != nil && err == sql.ErrNoRows {
		_, err := db.Exec("INSERT INTO ?(Name) values (?)", tableCodeTypes, codeType)
		if err != nil {
			log.Fatalf("Could not add codetype %s to %s table\n", codeType, tableCodeTypes)
		}
		_, err = db.Exec("CREATE TABLE IF NOT EXISTS ? (entry STRING PRIMARY KEY, code STRING)", codeType)
		if err != nil {
			log.Fatalf("Could not create table %s in database\n", codeType)
		}
		log.Printf("Added codeType '%s' to %s table\n", codeType, tableCodeTypes)
	} else {
		if err != nil {
			log.Fatalf("Got error with unexpected type: %v\n", err)
		}
	}

	_, err = db.Exec("INSERT INTO ? (Entry, Code) values (?, ?)", codeType, entry, value)
	if err != nil {
		log.Fatalf("Got error from expression: %s", codeType)
	}

	return err
}

func (sbsq *storageBackendSqlite) RemoveCode(codeType string, entry string) error {
	db, err := sbsq.openDb()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	return nil
}

func (sbsq *storageBackendSqlite) openDb() (*sql.DB, error) {
	db, err := sql.Open("sqlite3", sbsq.DbFilePath)
	if err != nil {
		return nil, fmt.Errorf("Error occured when trying to open sqlite3 database '%s'", sbsq.DbFilePath)
	}

	// _, err = db.Exec("CREATE TABLE IF NOT EXISTS ? (name STRING PRIMARY KEY)", tableCodeTypes)
	_, err = db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (name STRING PRIMARY KEY)", tableCodeTypes))
	if err != nil {
		db.Close()
		return nil, fmt.Errorf("Error occured when trying to ensure existence of '%s' table: %v\n", tableCodeTypes, err)
	}

	return db, nil
}
