package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"projectcodes/projectcodemanagement"
	"text/tabwriter"
)

// ============================================================================
// == Handler for LIST command ================================================

type cmdListHandler struct{}

func (h cmdListHandler) Description() string {
	return "List all code entries of a chosen type."
}

func (h cmdListHandler) Handle(dataPath string, args []string) {
	cmdFlags.Init("LIST", flag.ExitOnError)
	var cmdListCodeType = cmdFlags.String("codetype", "categories", "type of codes to list")
	cmdFlags.Parse(args)

	if *cmdShowHelp {
		h.printHelpMessage()
		os.Exit(0)
	}

	if len(*cmdUserDataPath) > 0 {
		dataPath = *cmdUserDataPath
	}

	records, err := projectcodemanagement.ListCodes(*cmdStorageEngine, dataPath, *cmdListCodeType)
	if err != nil {
		log.Fatal(err)
	}

	tabFmt := "%v\t%v\t\n"
	tw := tabwriter.NewWriter(os.Stdout, 0, 8, 2, ' ', tabwriter.Debug)
	fmt.Fprintf(tw, tabFmt, "Entry", "Code")
	fmt.Fprintf(tw, tabFmt, "----------", "--------")
	for _, ce := range records {
		fmt.Fprintf(tw, tabFmt, ce.Entry, ce.Code)
	}
	tw.Flush()
}

func (h cmdListHandler) printHelpMessage() {
	fmt.Printf("NAME:\n   ProjectCodeManager add - Manager for project codes, %s\n", h.Description())
	fmt.Printf("\nUSAGE:\n   %s list [options]\n", os.Args[0])
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
