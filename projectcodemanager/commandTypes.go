package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"projectcodes/projectcodemanagement"
)

// ============================================================================
// == Handler for TYPES command ===============================================

type cmdTypesHandler struct{}

func (h cmdTypesHandler) Description() string {
	return "List available code types."
}

func (h cmdTypesHandler) Handle(dataPath string, args []string) {
	cmdFlags.Init("TYPES", flag.ExitOnError)
	cmdFlags.Parse(args)

	if *cmdShowHelp {
		h.printHelpMessage()
		os.Exit(0)
	}

	if len(*cmdUserDataPath) > 0 {
		dataPath = *cmdUserDataPath
	}

	codetypes, err := projectcodemanagement.ReadCodeTypes(*cmdStorageEngine, dataPath)
	if err != nil {
		log.Fatal(err)
	}

	for i, ct := range codetypes {
		fmt.Printf("%03d: %s\n", i, ct)
	}
}

func (h cmdTypesHandler) printHelpMessage() {
	fmt.Printf("NAME:\n   ProjectCodeManager add - Manager for project codes, %s\n", h.Description())
	fmt.Printf("\nUSAGE:\n   %s list [options]\n", os.Args[0])
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
