package main

import (
	"flag"
)

type commandHandler interface {
	Description() string
	Handle(dataPath string, args []string)
}

var cmdFlags *flag.FlagSet = flag.NewFlagSet("BASE", flag.ExitOnError)
var cmdUserDataPath = cmdFlags.String("datapath", "", "custom data file(s) path")
var cmdStorageEngine = cmdFlags.String("engine", "files", "data storage type to use: files, sqlite")
var cmdShowHelp = cmdFlags.Bool("help", false, "print help for selected command message")
