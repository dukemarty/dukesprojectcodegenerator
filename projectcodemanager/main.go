package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var commandsHandlers map[string]commandHandler = map[string]commandHandler{
	"TYPES": cmdTypesHandler{},
	"LIST":  cmdListHandler{},
	"ADD":   cmdAddHandler{},
	"REM":   cmdRemoveHandler{},
}

func main() {

	if len(os.Args) < 2 {
		fmt.Println("ERROR: A command has to be provided!")
		printHelpMessage()
		os.Exit(1)
	}

	dataPath, err := determineDataPath()
	if err != nil {
		log.Println("Could not determine required data path.")
		os.Exit(2)
	}

	fmt.Println("Project Code Manager")

	cmd := os.Args[1]
	handler, ok := commandsHandlers[strings.ToUpper(cmd)]
	if !ok {
		fmt.Fprintf(os.Stderr, "Command '%s' can not be handled!\n", cmd)
		printHelpMessage()
		os.Exit(3)
	}

	handler.Handle(dataPath, os.Args[2:])
}

func determineDataPath() (string, error) {
	execPath, err := os.Executable()
	if err != nil {
		log.Println("Could not determine path to executable")
		execPath = "."
	}
	absPath, err := filepath.Abs(execPath)
	if err != nil {
		return "", err
	}

	log.Printf("absPath: %s\n", absPath)
	basePath, _ := filepath.Split(absPath)
	log.Printf("basePath: %s\n", basePath)
	dataPath := filepath.Join(basePath, "data")
	log.Printf("Determined data path: %s\n", dataPath)

	return dataPath, nil
}

func printHelpMessage() {
	fmt.Println("NAME:\n   ProjectCodeManager - Tool to manage a data store of project and other codes")
	fmt.Printf("\nUSAGE:\n   %s command [options]\n", os.Args[0])
	fmt.Println("\nCOMMANDS:")
	for cmd := range commandsHandlers {
		fmt.Printf("   %-8s%s\n", cmd, commandsHandlers[cmd].Description())
	}
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
