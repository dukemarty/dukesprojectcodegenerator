package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"projectcodes/projectcodemanagement"
)

// ============================================================================
// == Handler for ADD command =================================================

type cmdAddHandler struct{}

func (h cmdAddHandler) Description() string {
	return "Insert existing code into database."
}

func (h cmdAddHandler) Handle(dataPath string, args []string) {
	cmdFlags.Init("ADD", flag.ExitOnError)
	var cmdAddCodeType = cmdFlags.String("codetype", "categories", "type of code to add")
	var cmdAddEntry = cmdFlags.String("entry", "", "name of entry to add")
	var cmdAddCode = cmdFlags.String("code", "", "code value of entry to add")
	cmdFlags.Parse(args)

	if *cmdShowHelp {
		h.printHelpMessage()
		os.Exit(0)
	}

	if len(*cmdUserDataPath) > 0 {
		dataPath = *cmdUserDataPath
	}

	if len(*cmdAddEntry) == 0 || len(*cmdAddCode) == 0 {
		fmt.Fprintf(os.Stderr, "No data provided to ADD.\n")
		os.Exit(4)
	}

	log.Printf("Selected codetype for adding to: %s\n", *cmdAddCodeType)

	err := projectcodemanagement.AddCode(*cmdStorageEngine, dataPath, *cmdAddCodeType, *cmdAddEntry, *cmdAddCode)
	if err != nil {
		log.Fatalf("Got error from trying to add code: %s", err)
	}
}

func (h cmdAddHandler) printHelpMessage() {
	fmt.Printf("NAME:\n   ProjectCodeManager add - Manager for project codes, %s\n", h.Description())
	fmt.Printf("\nUSAGE:\n   %s add [options]\n", os.Args[0])
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
