package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"projectcodes/projectcodemanagement"
)

// ============================================================================
// == Handler for REMOVE command ==============================================

type cmdRemoveHandler struct{}

func (h cmdRemoveHandler) Description() string {
	return "Remove a code entry from the database."
}

func (h cmdRemoveHandler) Handle(dataPath string, args []string) {
	cmdFlags.Init("REMOVE", flag.ExitOnError)
	var cmdRemoveCodeType = cmdFlags.String("codetype", "categories", "type of code to remove")
	var cmdRemoveEntry = cmdFlags.String("entry", "", "name of entry to remove")
	cmdFlags.Parse(args)
	if len(*cmdUserDataPath) > 0 {
		dataPath = *cmdUserDataPath
	}

	if len(*cmdRemoveEntry) == 0 {
		fmt.Printf("No data provided to REMOVE.\n")
		os.Exit(4)
	}

	log.Printf("Selected codetype for removing: %s\n", *cmdRemoveCodeType)

	err := projectcodemanagement.RemoveCode(*cmdStorageEngine, dataPath, *cmdRemoveCodeType, *cmdRemoveEntry)
	if err != nil {
		log.Fatalf("Got error from trying to remove code: %s", err)
	}
}

func (h cmdRemoveHandler) printHelpMessage() {
	fmt.Printf("NAME:\n   ProjectCodeManager add - Manager for project codes, %s\n", h.Description())
	fmt.Printf("\nUSAGE:\n   %s rem [options]\n", os.Args[0])
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
