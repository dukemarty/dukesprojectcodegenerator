package projectdatahandling

import (
	"fmt"
	"io/ioutil"
	"strings"
)

// Represent the information describing a project.
//
// It is not to be considered uncommon that not all of the data fields are set,
// as depending on the usage, only a subset is required/interesting/even available.
type ProjectInfo struct {
	ProjectNumber         string
	WorkingTitle          string
	ProjectCode           string
	ProjectInfoHash       string
	DescriptionAndPurpose string
	VisionAndOutcome      string
}

func CreateProjectInfoFromFile(filename string) (ProjectInfo, error) {
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		return ProjectInfo{}, fmt.Errorf("Could not load project data file '%s'", filename)
	}

	return CreateProjectInfoFromString(string(buf))
}

func CreateProjectInfoFromString(content string) (ProjectInfo, error) {
	res := ProjectInfo{}

	for _, line := range strings.Split(string(content), "\r\n") {
		tokens := strings.Split(line, "=")
		if len(tokens) < 2 {
			continue
		}
		key, value := strings.TrimSpace(tokens[0]), strings.TrimSpace(tokens[1])
		switch strings.ToUpper(key) {
		case "PROJECTNUMBER":
			res.ProjectNumber = value
		case "WORKINGTITLE":
			res.WorkingTitle = value
		case "PROJECTCODE":
			res.ProjectCode = value
		case "PROJECTINFOHASH":
			res.ProjectInfoHash = value
		case "DESCRIPTION":
			fallthrough
		case "PURPOSE":
			res.DescriptionAndPurpose = value
		case "VISION":
			fallthrough
		case "OUTCOME":
			res.VisionAndOutcome = value
		}
	}

	return res, nil
}
