package main

import (
	_ "embed"
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
	"projectcodes/pcutils/archiveutil"
	"projectcodes/pcutils/fileutil"
)

const rawCoverTemplateName string = "RawCoverTemplate.odt"
const repackedCoverTemplateName string = "RepackedCoverTemplate.odt"

//go:embed data/CoverPageTemplate.odt
var XcoverpageTemplate []byte

type CoverTemplate struct {
	TempDirPath        string
	TemplateFilePath   string
	ContentExtractPath string
	RepackPath         string
}

func CreateCoverTemplate(template string) CoverTemplate {
	var tempDir string
	var err error
	if tempDir, err = os.MkdirTemp(path.Join(".", "TEST"), "PCPG-*"); err != nil {
		log.Fatal("Could not create temp dir for building cover page")
	}

	res := CoverTemplate{TempDirPath: tempDir, TemplateFilePath: filepath.Join(tempDir, rawCoverTemplateName)}

	if fileutil.FileExists(template) {
		input, err := ioutil.ReadFile(template)
		if err != nil {
			log.Fatalf("Error trying to read template from %s: %o", template, err)
		}

		err = ioutil.WriteFile(res.TemplateFilePath, input, 0644)
		if err != nil {
			log.Fatalf("Error writing template to %s: %o", rawCoverTemplateName, err)
		}
	} else {
		err = ioutil.WriteFile(res.TemplateFilePath, XcoverpageTemplate, 0644)
		if err != nil {
			log.Fatalf("Error writing template to %s: %o", rawCoverTemplateName, err)
		}
	}

	return res
}

func (ct *CoverTemplate) ExtractContents() (string, error) {
	ct.ContentExtractPath = filepath.Join(ct.TempDirPath, "RawDecompressed")
	err := archiveutil.ExtractZipContents(ct.TemplateFilePath, ct.ContentExtractPath)
	if err != nil {
		return "", err
	}

	return ct.ContentExtractPath, nil
}

func (ct *CoverTemplate) RepackContents(filename string) (string, error) {
	if filename == "" {
		ct.RepackPath = path.Join(".", "TEST", repackedCoverTemplateName)
	} else {
		ct.RepackPath = filename
	}

	err := archiveutil.CompressFolderToZip(ct.ContentExtractPath, ct.RepackPath)
	if err != nil {
		return "", err
	}

	return ct.RepackPath, nil
}

func (ct CoverTemplate) ClearAll() {
	err := os.RemoveAll(ct.TempDirPath)
	if err != nil {
		log.Printf("Error when trying to remove extracted cover from %s: %s\n", ct.TempDirPath, err)
	}
	err = os.Remove(ct.RepackPath)
	if err != nil {
		log.Printf("Error when trying to remove extracted cover from %s: %s\n", ct.TempDirPath, err)
	}
}
