package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"projectcodes/pcutils/fileutil"
	projectdata "projectcodes/projectdatahandling"
)

var showHelp = flag.Bool("help", false, "print help message")
var filename = flag.String("output", "ProjectCover.odt", "name of the output cover page file")
var template = flag.String("template", "", "full path to custom template odt file")
var projectinfo = flag.String("pinfo", "", "project info file name")

func main() {
	// 1) Argument parsing and checking as far as required
	flag.Parse()

	if err := checkRequiredArguments(); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		fPrintHelpMessage(os.Stderr)
		os.Exit(1)
	}

	if *showHelp {
		fPrintHelpMessage(os.Stdout)
		os.Exit(0)
	}

	// 2) Load project data and create cover
	projectInfo, err := projectdata.CreateProjectInfoFromFile(*projectinfo)
	if err != nil {
		log.Fatalf("Error when trying to load project information from file '%s': %s\n", *projectinfo, err)
	}
	fmt.Printf("Read project information:\n%#v\n", projectInfo)

	cover, err := CreateCover(*template, projectInfo)
	if err != nil {
		log.Fatalf("Could not create adapted cover page: %s\n", err)
	}
	defer cover.Close()

	// 3) Deploy created cover
	if err := cover.DeployCover(*filename); err != nil {
		log.Fatalf("Could not deploy cover page: %s\n", err)
	}
}

func checkRequiredArguments() error {
	if strings.TrimSpace(*filename) == "" {
		return errors.New("--output flag (filename) has to be provided!")
	}

	if strings.TrimSpace(*projectinfo) == "" {
		return errors.New("--pinfo flag (filename) has to be provided!")
	}

	if !fileutil.FileExists(*projectinfo) {
		return fmt.Errorf("Provided project info file '%s' does not exist!", *projectinfo)
	}

	return nil
}

func fPrintHelpMessage(w io.Writer) {
	fmt.Fprintf(w, "Copyright @Martin Lösch 2022, All Rights Reserved\n")
	fmt.Fprintf(w, "%s [OPTIONS]\n", os.Args[0])
	fmt.Fprintf(w, "Tool to generate a project cover page.\n")
	flag.PrintDefaults()
}
