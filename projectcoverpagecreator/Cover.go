package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"projectcodes/pcutils/fileutil"
	projectdata "projectcodes/projectdatahandling"
	"strings"
)

type Cover struct {
	Template        CoverTemplate
	Info            projectdata.ProjectInfo
	AdaptedFilePath string
}

func CreateCover(template string, projectInfo projectdata.ProjectInfo) (Cover, error) {
	res := Cover{Info: projectInfo}

	res.Template = CreateCoverTemplate(template)

	extractPath, err := res.Template.ExtractContents()
	if err != nil {
		return res, err
	}

	res.adaptTemplateWithProjectInfo(extractPath)
	if err != nil {
		return res, err
	}

	res.AdaptedFilePath, err = res.Template.RepackContents("")

	return res, err
}

func (cover Cover) DeployCover(targetFile string) error {

	fmt.Println("What to do for deploy? Copy created cover to destination")

	err := fileutil.CopyFile(cover.AdaptedFilePath, targetFile)

	return err
}

func (cover Cover) Close() {
	cover.Template.ClearAll()
}

func (cover Cover) adaptTemplateWithProjectInfo(extractPath string) error {
	contentFilenane := filepath.Join(extractPath, "content.xml")
	rawContent, err := ioutil.ReadFile(contentFilenane)
	if err != nil {
		return err
	}
	textContent := string(rawContent)
	newContent := strings.Replace(textContent, "{{ProjectCode}}", cover.Info.ProjectCode, -1)
	newContent = strings.Replace(newContent, "{{ProjectNumber}}", cover.Info.ProjectNumber, -1)
	newContent = strings.Replace(newContent, "{{WorkingTitle}}", cover.Info.WorkingTitle, -1)
	newContent = strings.Replace(newContent, "{{ProjectInfoHash}}", cover.Info.ProjectInfoHash, -1)
	newContent = strings.Replace(newContent, "{{Description/Purpose}}", cover.Info.DescriptionAndPurpose, -1)
	newContent = strings.Replace(newContent, "{{Vision/Outcome}}", cover.Info.VisionAndOutcome, -1)
	err = os.WriteFile(contentFilenane, []byte(newContent), 0644)
	if err != nil {
		return err
	}

	return nil
}
