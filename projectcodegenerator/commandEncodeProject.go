package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"projectcodes/projectcodehandling"
	"projectcodes/projectdatahandling"
)

type cmdEncProjHandler struct{}

func (h cmdEncProjHandler) Description() string {
	return "Generate code(s) for structured project data."
}

func (h cmdEncProjHandler) Handle(args []string) {
	cmdFlags.Init("PROJ", flag.ExitOnError)
	var cmdProjInfoFile = cmdFlags.String("projectfile", "", "data file containing project info to encode")
	var cmdProjInteractiveMode = cmdFlags.Bool("interactive", false, "start in interactive mode")
	var cmdProjProjectNumber = cmdFlags.String("projectnumber", "", "project number to encode")
	var cmdProjWorkingTitle = cmdFlags.String("workingtitle", "", "working title to encode")
	cmdFlags.Parse(args)

	if *cmdShowHelp {
		h.printHelpMessage()
		os.Exit(0)
	}

	dataPath := ""
	if len(*cmdUserDataPath) > 0 {
		dataPath = *cmdUserDataPath
		log.Printf("Changed data path to: %s\n", dataPath)
	}

	projectInfo, err := acquireProjectData(*cmdProjInteractiveMode, *cmdProjInfoFile, *cmdProjProjectNumber, *cmdProjWorkingTitle)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not acquire project data to encode: %s\n", err)
		os.Exit(3)

	}

	codes := projectcodehandling.GenerateCodesForProjectInfo(dataPath, projectInfo)
	processCodes(codes)
}

func acquireProjectData(interactiveMode bool, projectFile string, projectNumber string, workingTitle string) (projectdatahandling.ProjectInfo, error) {
	if interactiveMode {
		projectInfo := projectdatahandling.ProjectInfo{}
		fmt.Printf("Project number: ")
		_, _ = fmt.Scanf("%s\n", &projectInfo.ProjectNumber)
		fmt.Printf("Working title : ")
		_, _ = fmt.Scanf("%s\n", &projectInfo.WorkingTitle)
		return projectInfo, nil
	}

	if projectFile != "" {
		pinfo, err := projectdatahandling.CreateProjectInfoFromFile(projectFile)
		if err != nil {
			fmt.Printf("Could not load project data from file '%s'\n", projectFile)
			os.Exit(3)
		}

		return pinfo, nil
	}

	if projectNumber != "" && workingTitle != "" {
		res := projectdatahandling.ProjectInfo{
			ProjectNumber: projectNumber,
			WorkingTitle:  workingTitle,
		}

		return res, nil
	}

	return projectdatahandling.ProjectInfo{}, fmt.Errorf("")
}

func (h cmdEncProjHandler) printHelpMessage() {
	fmt.Printf("NAME:\n    ProjectCodeGenerator proj - Generator of project codes, %s\n", h.Description())
	fmt.Printf("\nUSAGE:\n    %s proj [options]\n", os.Args[0])
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
