/*
projectcodegenerator creates codes for identifying, referencing and speaking about projects

Usage:

	projectcodegenerator [flags]

The flags are:
*/
package main

import (
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	"projectcodes/projectcodehandling"

	"github.com/atotto/clipboard"
)

var commandsHandlers map[string]commandHandler = map[string]commandHandler{
	"RAW":  cmdEncRawHandler{},
	"PROJ": cmdEncProjHandler{},
}

func main() {

	if len(os.Args) < 2 {
		fmt.Println("ERROR: A command has to be provided!")
		printHelpMessage()
		os.Exit(1)
	}

	fmt.Println("Project Code Generator")

	cmd := os.Args[1]
	handler, ok := commandsHandlers[strings.ToUpper(cmd)]
	if !ok {
		fmt.Fprintf(os.Stderr, "Command '%s' can not be handled!\n", cmd)
		os.Exit(2)
	}

	handler.Handle(os.Args[2:])
}

func processCodes(codes []projectcodehandling.Code) {
	fmt.Println("Codes:\n======")
	printCodes(codes)
	copyCodesToClipboard(codes)
}

// Print all codes from the parameter list in a formatted way to stdout.
func printCodes(codes []projectcodehandling.Code) {
	tw := new(tabwriter.Writer).Init(os.Stdout, 0, 8, 2, ' ', tabwriter.Debug)
	const tabFormat = "%v\t%v\t\n"

	fmt.Fprintf(tw, tabFormat, "Type", "Code")
	fmt.Fprintf(tw, tabFormat, "----", "----")
	for _, c := range codes {
		// fmt.Printf("%s :  %s\n", c.Type, c.Code)
		fmt.Fprintf(tw, tabFormat, c.Type, c.Code)
	}
	tw.Flush()
}

// Write all codes from the parameter list comma-separated to the system's clipboard.
func copyCodesToClipboard(codes []projectcodehandling.Code) {
	var sb strings.Builder

	for i, c := range codes {
		if i > 0 {
			sb.WriteString(",")
		}
		sb.WriteString(c.Code)
	}

	clipboard.WriteAll(sb.String())
}

func printHelpMessage() {
	fmt.Println("NAME:\n    ProjectCodeGenerator - Tool to generate codes/code names in a reproducible way")
	fmt.Printf("\nUSAGE:\n    %s command [options]\n", os.Args[0])
	fmt.Println("\nCOMMANDS:")
	for cmd := range commandsHandlers {
		fmt.Printf("    %-8s%s\n", cmd, commandsHandlers[cmd].Description())
	}
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
