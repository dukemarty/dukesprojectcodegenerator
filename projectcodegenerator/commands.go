package main

import "flag"

type commandHandler interface {
	Description() string
	Handle(args []string)
}

var cmdFlags *flag.FlagSet = flag.NewFlagSet("BASE", flag.ExitOnError)
var cmdUserDataPath = cmdFlags.String("datapath", "", "custom data files path, if none is provided the embedded data files are used")
var cmdShowHelp = cmdFlags.Bool("help", false, "print help for selected command message")
