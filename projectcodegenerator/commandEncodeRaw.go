package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"projectcodes/projectcodehandling"
)

type cmdEncRawHandler struct{}

func (h cmdEncRawHandler) Description() string {
	return "Generate code for a raw data string."
}

func (h cmdEncRawHandler) Handle(args []string) {
	cmdFlags.Init("RAW", flag.ExitOnError)
	var cmdRawDataString = cmdFlags.String("rawstring", "", "raw string to encode")
	var cmdRawDataFile = cmdFlags.String("rawdatafile", "", "raw data file to encode")
	cmdFlags.Parse(args)

	if *cmdShowHelp {
		h.printHelpMessage()
		os.Exit(0)
	}

	dataPath := ""
	if len(*cmdUserDataPath) > 0 {
		dataPath = *cmdUserDataPath
		log.Printf("Changed data path to: %s\n", dataPath)
	}

	rawData, err := acquireRawData(*cmdRawDataString, *cmdRawDataFile)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not acquire raw data to encode: %s\n", err)
		os.Exit(3)
	}

	codes := projectcodehandling.GenerateCodesForString(dataPath, rawData)
	processCodes(codes)
}

func acquireRawData(rawString string, rawDataFile string) (string, error) {
	if rawString != "" {
		return rawString, nil
	}

	if rawDataFile != "" {
		buf, err := ioutil.ReadFile(rawDataFile)
		if err != nil {
			return "", fmt.Errorf("Could not load data from file '%s': %s", rawDataFile, err)
		}

		return string(buf), nil
	}

	return "", fmt.Errorf("No raw data provided for encoding!")
}

func (h cmdEncRawHandler) printHelpMessage() {
	fmt.Printf("NAME:\n    ProjectCodeGenerator raw - Generator of project codes, %s\n", h.Description())
	fmt.Printf("\nUSAGE:\n    %s raw [options]\n", os.Args[0])
	fmt.Println("\nOPTIONS:")
	cmdFlags.PrintDefaults()
}
